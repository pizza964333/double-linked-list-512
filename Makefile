all:
	mkdir -p tmp
	expand test01.c > test01.c.tmp
	(diff test01.c test01.c.tmp > /dev/null && mv test01.c.tmp test01.c) || rm -f test01.c.tmp
	cat stackSyntax.peg.macro | awk '/^#sh/ { print(gensub(/^..../,"","g")); next } { printf("cat <<EOF\n%s\nEOF\n", $$0) }' | sh | expand -t 24 > stackSyntax.peg
	cat stackSyntax.peg
	peg stackSyntax.peg > tmp/stackSyntax.c
	gcc test01.c -lgmp -lunistring && ./a.out


