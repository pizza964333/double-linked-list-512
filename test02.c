
#include "linkNet384mb.h"
#include "linkNet384mbAgentTypes.h"

Agent *linkNet; // 384 megabytes for agents of interaction net

int main(int argc, char *argv[])
{
	printf("%d\n", sizeof(Agent));
	printf("%d\n", sizeof(mpz_t));
	printf("%d\n", sizeof(Value64) * 8);
	printf("%d\n", sizeof(Links96) * 8);
	printf("%d\n", sizeof(DynType32) * 8);
	linkNet = calloc(sizeof(Agent),16777216);
	printf("%p\n", linkNet);
	return 0;
}


