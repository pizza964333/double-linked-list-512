
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <gmp.h>

#include "linkNet384mbLinkSchemes.h"

typedef union __attribute__((packed)) {
	uint64_t uint64V;
	double   doubleV;
	mpz_t   *bigIntV;
} Value64;

typedef struct __attribute__((packed)) {
	union  __attribute__((packed)) {
		uint16_t hint; // array parameters for example
	};
	union  __attribute__((packed)) {
		uint16_t type;
		struct __attribute__((packed)) {
			uint16_t linkScheme : 2; // 0 NoLinks, 1 TALR, 2 ALTR, 3 ALRT
			/*
			uint16_t isQuoted   : 1;
			uint16_t isOpCode   : 1;
			uint16_t isPureOp   : 1;
			uint16_t arityIn    : 4;
			uint16_t arityOut   : 4;
			uint16_t opcodeBits : 5;
			*/
		};
		struct __attribute__((packed)) {
			uint16_t skip02     : 2;
			uint16_t agentType  : 14;
		};
	};
} DynType32;

typedef struct __attribute__((packed)) {
	union  __attribute__((packed)) {
		struct __attribute__((packed)) {
			Value64   val64;
			Links96   lnk96;
		};
	};
	DynType32 dnt32;
} Agent;


