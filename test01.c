/*

 5  bit for type
 9  bit for left  link
 9  bit for right link
 9  bit for attic link, element hidding and plugging, opHIDE can be used instead DROP opcode, and element can be plugged back by opPLUG operator
 32 bit for value

 total 64 bits for stack element, 5 bits of dynamic type can be used to indicate different data structures and op codes

 if value is op code

 op code id   9 bits
 quoted state 1 bit, like in joy stack quote can be used to isolate opcodes for example SWAP operation for opcodes
 arity in     3 bits
 arity out    3 bits

 if type is pointer
 9 + 9 + 9 + 32 = 59 bits used for pointer

 if left link and right link is just one to one array in memory, than optimized stack operations can be used like in forth
 left and right links is just equal to 256 that indicates just stack one by one of 64 bit elements
 classic forth operations for SWAP ROT can be used
 just pop elements from 64 bit stack and push it back in other order

 if 9 bit for left right or attic links is not enouth for locating stack element TE_Pointer can be used to store 59 bit pointers

 summations of all this features is abiliry to use classic forth style, joy style, hiding style, long pointers, javascript objects at same time
 quick forth stack operating can be used and we not loose ability to use term rewriting style like in joy
 in case of term rewriting we can implement pi calculus opcodes and linear logic

 with hide and plug opcodes we can use proof systems in style of mathematical reduction of elements
 and we can rewrite stack back to some number of steps and try another proof for other combination

 we can use big numbers provided by gmp functions and get ability of cryptoprimitives like in v8 virtual machine

 in case of double linked list style stack be can use patrtial reduction of mathematical terms
 and we can remove space between links after this and get simple stack like in forth

 side effects of operations is not posible for partial redictions, but in forth stack it is posible with correct order



 */

#include <stdint.h>
#include <stdlib.h>
#include <gmp.h>
#include <stdio.h>
#include <limits.h>
#include <ctype.h>
#include <string.h>

#include <unistr.h>
#include <uniconv.h>
#include <unistdio.h>
#include <uniname.h>
#include <unictype.h>
#include <uniwidth.h>
#include <uniwbrk.h>
#include <unilbrk.h>
#include <uninorm.h>
#include <unicase.h>
#include <inttypes.h>

#include <locale.h>

typedef enum
{
        TE_Unused,
        TE_Pointer,
        TE_Word32,
        TE_Int32,
        TE_Float32,
        TE_LinkToStack,
        TE_OpCode,
        TE_Quote,
        TE_BigInteger,
        TE_ByteArray,
        TE_Object,
        TE_Array,
        TE_String,
        TE_Json,
        TE_Iterator,
        TE_UserData,
        TE_Error,
        TE_True,
        TE_False,
        TE_Function,
        TE_Undefined,
        TE_Null,
        TE_Regexp,
	TE_Symbol,
} CatiType;

enum CatiOpCode
{
        OP_Unused,
        OP_SWAP,
        OP_BITAND,
        OP_ADD,
        OP_HIDE,
};

typedef struct {
        uint32_t cdscds;
} LockSystem;

//#define INLINE inline __attribute__((always_inline))
#define INLINE

INLINE void lock(LockSystem *lcks)
{
        lcks++;
}

INLINE void unlock(LockSystem *lcks)
{
        lcks--;
}

typedef struct {
        uint32_t type  : 5;
        uint32_t left  : 9;
        uint32_t right : 9;
        uint32_t attic : 9;
        union {
                uint32_t value;
        };
} STElem;

uint32_t qhashMix(uint32_t value, uint32_t state)
{
	uint64_t a = value*value;
	uint64_t b = a*value;
	uint64_t c = b*value;
	uint64_t d = state*state;
	uint64_t e = d*state;
	uint64_t f = e*state;

	//uint64_t a = (value+state+1)*(value+state)/2+value;
	//uint64_t b = (value*value - a*a*a) % (state*state*state - a*a);
	return value + state * 2 + a * 3 + b * 5 + c * 7 + d * 13 + e * 17 + f * 19;
}

INLINE STElem *getPrevSTElemP(STElem *stp01)
{
        STElem *stp02 = (STElem*) ( (uint64_t)stp01 + 1024 - ((uint64_t)(stp01->left)) * 4 );
        return stp02;
}

INLINE STElem *getNextSTElemP(STElem *stp01)
{
        STElem *stp02 = (STElem*) ( (uint64_t)stp01 + 1024 - ((uint64_t)(stp01->right)) * 4 );
        return stp02;
}

INLINE void setNextSTElem(STElem *a, STElem *b)
{
        a->right = ( (uint64_t)a + 1024 - (uint64_t)b ) / 4;
}

INLINE void setPrevSTElem(STElem *a, STElem *b)
{
        a->left = ( (uint64_t)a + 1024 - (uint64_t)b ) / 4;
}

INLINE CatiType getTypeOfSTElem(STElem *stp01)
{
        return stp01->type;
}

INLINE uint32_t getValueOfSTElem(STElem *stp01)
{
        return stp01->value;
}

INLINE void stElemSetLinks(STElem *a, STElem *b, STElem *c)
{
        a->left = ( (uint64_t)a + 1024 - (uint64_t)b ) / 4;
        a->right = ( (uint64_t)a + 1024 - (uint64_t)c ) / 4;
}

INLINE STElem *opHIDE(STElem *a)
{
        STElem *b = getPrevSTElemP(a);
        STElem *c = getNextSTElemP(a);
        setNextSTElem(b,c);
        return b;
}

STElem *getNextElemP(STElem *);

INLINE STElem *opPUSH(STElem *a, STElem *b)
{
        //STElem *c = getNextSTElemP(a);
        a->right = ( (uint64_t)a + 1024 - (uint64_t)b ) / 4;
        b->left  = ( (uint64_t)b + 1024 - (uint64_t)a ) / 4;
        //b->right = ( (uint64_t)b + 1024 - (uint64_t)c ) / 4;
        return b;
}

static uint64_t *steMemoryPool = NULL;

STElem *newSTElemP(STElem *middle, CatiType type, uint32_t value)
{
        STElem *elem = NULL;
        if (steMemoryPool == NULL) {
                steMemoryPool = (uint64_t*) calloc(9999,sizeof(STElem));
        }
        if (middle == NULL) {
                elem = (STElem*) &(steMemoryPool[500]);
        } else if ( ((uint64_t)middle - (uint64_t)steMemoryPool) > 9999*sizeof(STElem) ) {
                //printf("middle is from other memory pool\n");
        } else {
                uint64_t candidate;
                int locator = -20;
                do {
                        locator++;
                        candidate = ((uint64_t*)middle)[locator];
                        //printf("cand %d\n", candidate);
                } while (candidate != 0 && locator < 20);
                if (candidate == 0) {
                        elem = &(middle[locator]);
                } else {
                        //printf("out of link ability\n");
                }
        }
        if (elem != NULL) {
                elem->type = type;
                elem->value = value;
        }
        return elem;
}

STElem *opSWAP(LockSystem *lcks, STElem *stp01)
{
        lock(lcks);
        STElem *ap = stp01;
        STElem *bp = getPrevSTElemP(ap);
        STElem *cp = getPrevSTElemP(bp);
        stElemSetLinks(bp,ap,getNextSTElemP(ap));
        stElemSetLinks(ap,cp,bp);
        unlock(lcks);
        return bp;
}

STElem *opBITAND(LockSystem *lcks, STElem *stp01)
{

        lock(lcks);
        STElem *ap = stp01;
        STElem *bp = getPrevSTElemP(ap);
        CatiType ate = getTypeOfSTElem(ap);
        CatiType bte = getTypeOfSTElem(bp);
        switch (ate) {
                case TE_Word32:
                        switch (bte) {
                                case TE_Word32: {
                                        uint32_t av = getValueOfSTElem(ap);
                                        uint32_t bv = getValueOfSTElem(bp);
                                        uint32_t cv = av & bv;
                                        STElem *stp02 = opHIDE(opHIDE(stp01));
                                        STElem *stp03 = opPUSH(stp02,newSTElemP(stp01,TE_Word32, cv));
                                        unlock(lcks);
                                        return stp03;
                                                }
                                default:
                                        unlock(lcks);
                                        return stp01;
                        }
                default:
                        unlock(lcks);
                        return stp01;
        }
}

mpz_t *getMpzFromBigInteger(STElem *a)
{
        mpz_t *mpzA = (mpz_t*) ( (uint64_t) ( ((int64_t)a) + ((int32_t)a->value) ) );
        return mpzA;
}

void setMpzForBigInteger(STElem *ste01, mpz_t *mpzA)
{
        int64_t diff = ((int64_t)mpzA) - ((int64_t)ste01);
        if (abs(diff) > 1000000000) {
                //printf("pointer must be used\n");
        } else {
                //printf("we can use just 32 bit link\n");
                ste01->value = (int32_t)diff;
        }
}

STElem *getSTElemFromPointer(STElem *ste01)
{
        int64_t addr = (int64_t)ste01 + (int64_t)(((uint64_t)*((uint64_t*)ste01))>>5);
        return (STElem*)addr;
}

STElem *newBigInteger(STElem *middle, const char *str)
{
        mpz_t *mpzA = (mpz_t*) malloc(sizeof(mpz_t));
        mpz_init(*mpzA);
        mpz_set_str(*mpzA, str, 10);
        STElem *ste01 = newSTElemP(middle, TE_BigInteger, 0);

        int64_t diff = ((int64_t)mpzA) - ((int64_t)ste01);
        if (abs(diff) > 1000000000) {
                //printf("pointer must be used\n");
        } else {
                //printf("we can use just 32 bit link\n");
                ste01->value = (int32_t)diff;
                return ste01;
        }

}

//static (uint32_t***) symbolHashTable = NULL;

STElem *newSymbol(STElem *middle, uint32_t *utextTmp, size_t ulen)
{
	/*
	if (symbolHashTable == NULL) {
		symbolHashTable = calloc(sizeof(uint32_t**),65535);
	}
	int i;
	uint32_t hmst = 137137137;

	for (i=0; i<ulen; i++) {
		hmst = qhashMix(utextTmp[i], hmst);
	}

	uint16_t htpo = hmst & 0xffff;

	if (symbolHashTable[htpo] == NULL) {
		symbolHashTable[htpo] = malloc(sizeof(uint32_t*));
	}

	printf("hash %u\n", hmst);
	printf("hash %u\n", hmst & 0xff);
	*/

	uint32_t *utext = malloc(sizeof(uint32_t)*ulen+1);
	memcpy(utext, utextTmp, sizeof(uint32_t)*ulen);
	utext[ulen] = 0;
	STElem *ste01 = newSTElemP(middle, TE_Symbol, 0);
	int64_t diff = (int64_t)ste01 - (int64_t)utext;
	ste01->value = (uint32_t)((int32_t)diff);
	return ste01;
	//STElem *ste01 = newSTElemP(middle, TE_Symbol, 0);
	//return ste01;
}

STElem *opADD(LockSystem *lcks, STElem *stp01)
{

        lock(lcks);
        STElem *ap = stp01;
        STElem *bp = getPrevSTElemP(ap);
        CatiType ate;
        CatiType bte;
switchByType:
        ate = getTypeOfSTElem(ap);
        bte = getTypeOfSTElem(bp);
        switch (ate) {
                case TE_Pointer: { ap = getSTElemFromPointer(ap); goto switchByType; }
                case TE_Word32:
                        switch (bte) {
                                case TE_Pointer: { bp = getSTElemFromPointer(bp); goto switchByType; }
                                case TE_Word32:
                                {       uint32_t av = getValueOfSTElem(ap);
                                        uint32_t bv = getValueOfSTElem(bp);
                                        if ( (UINT_MAX - av) < bv ) {
                                          mpz_t *mpzC = (mpz_t*) malloc(sizeof(mpz_t));
                                          mpz_t  mpzA;
                                          mpz_init(*mpzC);
                                          mpz_init(mpzA);
                                          mpz_set_ui(mpzA, av);
                                          mpz_add_ui(*mpzC, mpzA, bv);
                                          STElem *stp02 = newSTElemP(stp01,TE_BigInteger, 0);
                                          setMpzForBigInteger(stp02, mpzC);
                                          STElem *stp03 = opHIDE(opHIDE(stp01));
                                          STElem *stp04 = opPUSH(stp03, stp02);
                                          return stp04;
                                        } else {
                                          STElem *stp02 = opHIDE(opHIDE(stp01));
                                          STElem *stp03 = opPUSH(stp02,newSTElemP(stp02,TE_Word32, av + bv));
                                          unlock(lcks);
                                          return stp03;
                                        } }
                                case TE_BigInteger:
                                {       mpz_t *mpzC = (mpz_t*) malloc(sizeof(mpz_t));
                                        mpz_t *mpzB = getMpzFromBigInteger(bp);
                                        mpz_init(*mpzC);
                                        mpz_add_ui(*mpzC, *mpzB, getValueOfSTElem(ap));
                                        STElem *stp02 = newSTElemP(stp01,TE_BigInteger, 0);
                                        setMpzForBigInteger(stp02, mpzC);
                                        STElem *stp03 = opHIDE(opHIDE(stp01));
                                        STElem *stp04 = opPUSH(stp03, stp02);
                                        return stp04; }
                                default: goto skipReduction;
                        }
                case TE_BigInteger:
                        switch (bte) {
                                case TE_Pointer: { bp = getSTElemFromPointer(bp); goto switchByType; }
                                case TE_Word32:
                                {       mpz_t *mpzC = (mpz_t*) malloc(sizeof(mpz_t));
                                        mpz_t *mpzA = getMpzFromBigInteger(ap);
                                        mpz_init(*mpzC);
                                        mpz_add_ui(*mpzC, *mpzA, getValueOfSTElem(bp));
                                        STElem *stp02 = newSTElemP(stp01,TE_BigInteger, 0);
                                        setMpzForBigInteger(stp02, mpzC);
                                        STElem *stp03 = opHIDE(opHIDE(stp01));
                                        STElem *stp04 = opPUSH(stp03, stp02);
                                        return stp04; }
                                case TE_BigInteger:
                                {       mpz_t *mpzC = (mpz_t*) malloc(sizeof(mpz_t));
                                        mpz_t *mpzA = getMpzFromBigInteger(ap);
                                        mpz_t *mpzB = getMpzFromBigInteger(bp);
                                        mpz_init(*mpzC);
                                        mpz_add(*mpzC, *mpzA, *mpzB);
                                        STElem *stp02 = newSTElemP(stp01,TE_BigInteger, 0);
                                        setMpzForBigInteger(stp02, mpzC);
                                        STElem *stp03 = opHIDE(opHIDE(stp01));
                                        STElem *stp04 = opPUSH(stp03, stp02);
                                        return stp04; }
                                default: goto skipReduction;
                        }
        }
skipReduction:
        unlock(lcks);
        return stp01;
}

void printStack(STElem *a)
{
        do {
                if (a->type == TE_BigInteger) {
                        mpz_t *mpzA = (mpz_t*) ( (uint64_t) ( ((int64_t)a) + ((int32_t)a->value) ) );
                        printf("type %d left %d right %d value %s\n", a->type, a->left, a->right, mpz_get_str(NULL, 10, *mpzA));
		} else if (a->type == TE_Symbol) {
			int32_t diff = a->value;
			//printf("diff %d\n", diff);
			uint32_t *utext = (uint32_t*) ((int64_t)a - diff);
			ulc_fprintf(stdout, "type %d left %d right %d value \"%llU\"\n", a->type, a->left, a->right, utext);
                } else {
                  printf("type %d value %u left %d right %d\n", a->type, a->value, a->left, a->right);
                }
                a = getPrevSTElemP(a);
                //printf("%p\n", a);
        } while( ((uint64_t*)a)[0] != 0 );
}

int main2(int argc, char *argv[])
{
        LockSystem *lcks = (LockSystem*) malloc(sizeof(LockSystem));
        STElem *a = newSTElemP(NULL, TE_Word32, 1234);
        STElem *b = newSTElemP(a, TE_Word32, 1235);
        STElem *c = newSTElemP(b, TE_Word32, 3243);
        STElem *d = newSTElemP(b, TE_OpCode, OP_BITAND);
        STElem *e = opPUSH(opPUSH(opPUSH(a,b),c),d);
        STElem *f = opHIDE(e);
        //printStack(opBITAND(lcks,f));
        printStack(opSWAP(lcks,f));
        STElem *g = newBigInteger(f,"2000000000000400000000000005000000000000000000000000");
        STElem *h = newBigInteger(g,"1000000000000100000000000001000000000000000000000000");
        STElem *k = opPUSH(opPUSH(f,g),h);
        printStack(k);
        STElem *m = opADD(lcks,k);
        printStack(m);
        STElem *n = opADD(lcks,opPUSH(m, newSTElemP(m, TE_Word32, 330999)));
        printStack(n);
        STElem *p = opSWAP(lcks,opPUSH(n, newSTElemP(n, TE_Word32, 779111)));
        printStack(p);
        STElem *q = opADD(lcks,p);
        printStack(q);
        STElem *q01 = opPUSH(opPUSH(q, newSTElemP(q, TE_Word32, 10000)), newSTElemP(q, TE_Word32, 999));
        printStack(q01);
        STElem *q02 = opADD(lcks,q01);
        printStack(q02);
        STElem *q03 = opPUSH(q02,newSTElemP(q02, TE_Word32, UINT_MAX));
        printStack(q03);
        STElem *q04 = opADD(lcks,q03);
        printStack(q04);
}

STElem *stack;
STElem *cursor;
int cmdnline = 0;
LockSystem *lcks;
uint32_t uutext[2048];
int uupos;
size_t uulen;
uint32_t uuchar;

void printPrompt(int a)
{
        printf("%08d> ", a);
}


STElem *evalStack(LockSystem *lcks, STElem *stp01)
{
  CatiType a = getTypeOfSTElem(stp01);
  uint32_t b = getValueOfSTElem(stp01);
  //printf("%d %d %d\n", a,b, b == OP_ADD);
  if (a == TE_OpCode) {
          switch (b) {
                case OP_ADD:    return opADD(lcks,opHIDE(stp01));
                case OP_SWAP:   return opSWAP(lcks,opHIDE(stp01));
                case OP_BITAND: return opBITAND(lcks,opHIDE(stp01));
                case OP_HIDE:   return opHIDE(stp01);
                default:   return stp01;
          }
  } else {
          return stp01;
  }
}

#include "tmp/stackSyntax.c"

int main(int argc, char *argv[])
{
	setlocale(LC_ALL,"en_US.UTF-8");
        lcks = (LockSystem*) malloc(sizeof(LockSystem));
        stack = newSTElemP(NULL, TE_Word32, 1000000);
        cursor = stack;
        cmdnline = 0;
	u8_to_u32("w",1,&uuchar,&uulen);
        printPrompt(cmdnline);
        while(yyparse()) {
                if (cmdnline == -1) {
                        printf("\n==== stack dump\n");
                        printStack(stack);
                        return 0;
                }
        }
        return 0;
}























