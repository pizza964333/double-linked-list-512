
typedef enum {
	SeqTLR, SeqLRT, SeqRTL,
	MulTLR, MulLRT, MulRTL,
	AtomInv, MatrInv,
	AtomMulPkt, AtomSeqPkt,
	OfCouseMulPkt, OfCouseSeqPkt,
} AgentType;


