
#include <stdint.h>

typedef union __attribute__((packed)) {

	/*
	union {
		struct {
			uint64_t attic   : 24;
			uint64_t top     : 24;
		};
		struct {
			uint16_t skip;
			uint64_t left    : 24;
			uint64_t right   : 24;
		};
	} ATLR;

	union {
		struct {
			uint64_t attic   : 24;
			uint64_t left    : 24;
		};
		struct {
			uint16_t skip;
			uint64_t top     : 24;
			uint64_t right   : 24;
		};
	} ALTR;
	*/

	union __attribute__((packed)) {
		struct __attribute__((packed)) {
			uint64_t attic   : 24;
			uint64_t left    : 24;
		};
		struct __attribute__((packed)) {
			uint16_t skip01;
			uint64_t skip02  : 32;
			uint64_t right   : 24;
		};
		struct __attribute__((packed)) {
			uint64_t skip03;
			uint32_t skip04  :  8;
			uint32_t top     : 24;
		};
	} ALRT;

} Links96;


